const Transmission = require('transmission');

/*
Torrent status
	STOPPED       : 0  # Torrent is stopped
  CHECK_WAIT    : 1  # Queued to check files
  CHECK         : 2  # Checking files
  DOWNLOAD_WAIT : 3  # Queued to download
  DOWNLOAD      : 4  # Downloading
  SEED_WAIT     : 5  # Queued to seed
  SEED          : 6  # Seeding
  ISOLATED      : 7  # Torrent can't find peers
*/

class Client {
	constructor(host, port, endpoint, user, password) {
		this.client = new Transmission({
			host,
			port: port || 9091,
			endpoint: endpoint || '/transmission/rpc',
			user: user || null,
			password: password || null,
		});
	}

	getVersion() {
		return new Promise((resolve, reject) => {
			this.client.get((err) => {
				if (err){
					reject(err);
				}
				else {
					resolve('Transmission');
				}
			});
			setTimeout(() => {reject(new Error('Timeout'))}, 2000);
		});
	}

	async transform(torrent, withFiles) {
		const data = {
			hash: torrent.hashString.toUpperCase(),
			name: torrent.name,
			active: [3,4,5,6].includes(torrent.status),
			downloaded: Number(torrent.downloadedEver),
			uploaded: Number(torrent.uploadedEver),
			length: Number(torrent.sizeWhenDone),
			ratio: torrent.uploadRatio,
			complete: Number(torrent.sizeWhenDone) === Number(torrent.downloadedEver),
			createdAt: (new Date(torrent.addedDate*1000)).toJSON(),
			free_space: await this.freeSpace(torrent.downloadDir),
			down_rate: torrent.rateDownload,
			extra: {
				ratio: torrent.uploadRatio,
				down_rate: Number(torrent.rateDownload),
				nb_seeders: torrent.seeders,
				nb_leechers: torrent.leechers,
				status: torrent.status,
			},
		};

		if(withFiles) {
			data.files = torrent.files.map((f) => ({
				name: f.name,
				length: f.length,
				isCompleted: f.bytesCompleted === f.length,
			}))
		}

		return data;
	}

	async test(hash) {
		return this.getFiles(hash);
	}

	async get(details, withFiles) {
		const self = this;
		return new Promise((resolve, reject) => {
			this.client.get(async(err, result) => {
				if (err){
					reject(err);
				} else {
					const torrents = [];
					for(let i=0; i<result.torrents.length; i++) {
						if(details) {
							torrents.push((await self.transform(result.torrents[i], withFiles)));
						} else {
							torrents.push(result.torrents[i].hashString.toUpperCase());
						}
					}
					resolve(torrents);
				}
			});
		});
	}

	getOne(hash, withFiles) {
		const self = this;
		return new Promise((resolve, reject) => {
			this.client.get(hash, (err, result) => {
				if (err){
					reject(err);
				}
				else {
					resolve(self.transform(result.torrents[0], withFiles));
				}
			});
		});
	}

	freeSpace(path) {
		return new Promise((resolve, reject) => {
			this.client.freeSpace(path, (err, result) => {
				if (err){
					reject(err);
				}
				else {
					resolve(result['size-bytes']);
				}
			});
		});
	}

	async getFiles(hash) {
		return new Promise((resolve, reject) => {
			this.client.get(hash, (err, result) => {
				if (err){
					reject(err);
				}
				else {
					resolve(result.torrents[0].files.map((f) => ({
						name: f.name,
						length: f.length,
						isCompleted: f.bytesCompleted === f.length,
					})));
				}
			});
		});
	}

	async play(hash) {
		return new Promise((resolve, reject) => {
			this.client.start(hash, function(err, arg) {
				if (err){
					reject(err);
				}
				else {
					resolve(arg);
				}
			});
		});
	}

	async pause(hash) {
		return new Promise((resolve, reject) => {
			this.client.stop(hash, function(err, arg) {
				if (err){
					reject(err);
				}
				else {
					resolve(arg);
				}
			});
		});
	}

	async remove(hash) {
		return new Promise((resolve, reject) => {
			this.client.remove(hash, false, function(err, arg) {
				if (err){
					reject(err);
				}
				else {
					resolve(arg);
				}
			});
		});
	}

	async createFromFile(file) {
		return new Promise((resolve, reject) => {
			this.client.addFile(file, function(err, arg) {
				if(err) {
					reject(err);
				} else {
					resolve(arg);
				}
			});
		});
	}

	async createFromBuffer(buffer) {
		throw new Error('"createFromBuffer" is not supported');
	}
}

module.exports = Client;