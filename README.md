# wTorrent - Transmission

Wrapper for Transmission (torrent server).

*This package is used for wrap generic methods for [wTorrent](https://www.npmjs.com/package/wtorrent)*

If you want complete wrapper for Transmission, use https://www.npmjs.com/package/transmission

## Usage

```javascript
const wTransmission = require('wtorrent-transmission');

const client = wTransmission({
  host: '127.0.0.1', // required
  port: 9091, // optional, default 9091
  endpoint: '/transmission/rpc', // optional, default /transmission/rpc
  user: 'admin', // optional, default null
  password: 'admin', // optional, default null
});

const torrents = await client.get();
const torrent = await client.getOne('TORRENT_HASH');
```